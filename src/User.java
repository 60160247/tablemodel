/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Peppa Point
 */
public class User {
    private String name;
    private String surname;
    private String username;
    private String password;
    private String tel;
    private double weight;
    private int height;

    public User() {
        
    }

    public User(String name, String surname, String username, String password, String tel, double weight, int height) {
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.password = password;
        this.tel = tel;
        this.weight = weight;
        this.height = height;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getTel() {
        return tel;
    }

    public double getWeight() {
        return weight;
    }

    public int getHeight() {
        return height;
    }
    
    
}
